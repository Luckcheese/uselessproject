package com.einstein.useless.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by rafaelduarte on 6/27/13.
 */
public class BaseFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        TextView feliz = new TextView(getActivity());
        feliz.setText("Uhuuuuuu");
        feliz.setGravity(Gravity.CENTER);
        return feliz;
    }
}
