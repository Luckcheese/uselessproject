package com.einstein.useless.activities;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.einstein.useless.R;

public class MainActivity extends SherlockFragmentActivity {

    public static String INTENT_TEXT_TO_SHOW = MainActivity.class.getPackage().getName() + "INTENT_TEXT_TO_SHOW";
    public static int sTimesSettingsWasClicked = 0;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();

        String textToShow = getIntent().getStringExtra(INTENT_TEXT_TO_SHOW);
        if (textToShow != null) {
            ((TextView)findViewById(android.R.id.text1)).setText(textToShow);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_settings:
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(INTENT_TEXT_TO_SHOW, String.format(getString(R.string.text_to_show_on_new_view), ++sTimesSettingsWasClicked));
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
